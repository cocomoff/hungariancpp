#include <iostream>
#include <vector>
#include <limits>

using namespace std;

#define REP(i,n) for(int i = 0; i < (int)(n); i++)
#define residue(i,j) (adj[i][j] + ofsleft[i] + ofsright[j])

vector<int> min_cost_match(vector<vector<int>> adj) {
    int n = adj.size();
    int m = adj[0].size();
    vector<int> toright(n, -1), toleft(m, -1);
    vector<int> ofsleft(n, 0), ofsright(m, 0);

    REP(r, n) {
        vector<bool> left(n, false), right(m, false);
        vector<int> trace(m, -1), ptr(m, r);
        left[r] = true;

        for(;;) {
            int d = numeric_limits<int>::max();
            REP(j, m) if (!right[j])
                d = min(d, residue(ptr[j], j));
            REP(i, n) if (left[i])
                ofsleft[i] -= d;
            REP(j, m) if (right[j])
                ofsright[j] += d;
            int b = -1;
            REP(j, m) if (!right[j] && residue(ptr[j], j) == 0)
                b = j;
            trace[b] = ptr[b];
            int c = toleft[b];
            if (c < 0) {
                while(b >= 0) {
                    int a = trace[b];
                    int z = toright[a];
                    toleft[b] = a;
                    toright[a] = b;
                    b = z;
                }
                break;
            }
            right[b] = left[c] = true;
            REP(j, m) if (residue(c, j) < residue(ptr[j], j))
                ptr[j] = c;
        }
    }
    return toright;
}


int main() {
  cout << "Hoge Piyo" << endl;

  int n, m;
  cin >> n >> m;
  cout << n << "," << m << endl;

  vector<vector<int>> adj;
  for (int i = 0; i < n; i++) {
    vector<int> v;
    for (int j = 0; j < m; j++) {
      int c;
      cin >> c;
      v.push_back(c);
    }
    adj.push_back(v);
  }
  vector<int> ans = min_cost_match(adj);
  for (auto v : ans) {
    cout << v << endl;
  }
  return 0;
}
